<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Title</title>

    <%--Css dependency--%>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet">

    <%--Js dependency--%>
    <script src="<c:url value='/static/js/lib/angular.js' />"></script>
    <script src="<c:url value='/static/js/app.js' />"></script>
    <script src="<c:url value='/static/js/service/indexService.js' />"></script>
    <script src="<c:url value='/static/js/controller/indexController.js' />"></script>
</head>
<body ng-app="myApp">

<!-- Page Content -->
<div class="container" ng-controller="IndexController as ctrl">

    <header class="jumbotron hero-spacer">
        <div class="row">
            <div class="center-block col-md-4" style="float: none; ">
                <div class="col-lg-6">
                    <div class="input-group">
                        <input type="date" class="form-control" ng-model="ctrl.datevalue">
                         <span class="input-group-btn">
                             <button class="btn btn-success" type="button" ng-click="ctrl.addWeather(ctrl.datevalue)">
                                 Add
                             </button>
                         </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="center-block col-md-4" style="float: none; ">
                <div class="col-lg-6 error">
                    {{ctrl.message}}
                </div>
            </div>

        </div>
    </header>
    <hr>

    <div class="row">
        <div class="col-lg-12">
            <h3>Weather forecast</h3>
        </div>
    </div>

    <div class="row text-center">

        <div class="col-md-3 col-sm-6 hero-feature" ng-repeat="item in ctrl.data">
            <div class="thumbnail">
                <div class="caption">
                    <h3>{{item.date | date:"dd/MMMM/yyyy"}}</h3>
                    <div ng-repeat="data in item.dataSet">
                        <p>{{data.day}}</p>
                        <p>{{data.weather}}</p>
                    </div>
                    <p></p>
                </div>
            </div>
        </div>

    </div>
    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; Vanches 2016</p>
            </div>
        </div>
    </footer>
</div>

</body>
</html>
