/**
 * User: Vanja Novak
 * Date: 02.05.2016
 * Time: 22:08
 */
'use strict';

App.factory('IndexService', ['$http', '$q', function ($http, $q) {
    return {
        getAllData: function () {
            return $http.get('http://localhost:8080/api/data/').then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    console.error('Error');
                    return $q.reject(errResponse);
                }
            )
        },

        createDate: function (date) {
            return $http.get('http://localhost:8080/api/data/' + date);
        }
    }
}]);