/**
 * User: Vanja Novak
 * Date: 02.05.2016
 * Time: 22:06
 */

App.controller('IndexController', ['$scope', 'IndexService', '$filter', function ($scope, IndexService, $filter) {
    var self = this;

    self.getAllData = function () {
        IndexService.getAllData()
            .then(
                function (response) {
                    self.data = response;
                },
                function (errResponse) {
                    console.error("Error");
                }
            )
    };

    self.addWeather = function (date) {

        var _date = $filter('date')(new Date(date), 'MM dd yyyy');

        IndexService.createDate(_date)
            .then(
                function (response) {
                    self.message = response.statusText;
                    if (response.status === 200) {
                        self.getAllData();
                    }
                }
            )
    };
    self.getAllData();
}]);