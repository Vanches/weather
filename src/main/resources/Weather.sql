-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 02 2016 г., 18:27
-- Версия сервера: 5.5.48
-- Версия PHP: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `Weather`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Weather`
--

CREATE TABLE IF NOT EXISTS `Weather` (
  `id` int(11) NOT NULL,
  `day` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `weather` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Weather`
--

INSERT INTO `Weather` (`id`, `day`, `date`, `weather`) VALUES
(1, 'NIGHT', '2016-05-09', '+10 +12'),
(2, 'DAY', '2016-05-09', '+8 +11');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Weather`
--
ALTER TABLE `Weather`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Weather`
--
ALTER TABLE `Weather`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
