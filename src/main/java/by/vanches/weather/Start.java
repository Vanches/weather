package by.vanches.weather;

import by.vanches.weather.model.Weather;
import by.vanches.weather.utils.DateParse;
import by.vanches.weather.utils.WeatherParser;
import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: Vanja Novak
 * Date: 01.05.2016
 * Time: 23:16
 */

public class Start {
    public static void main(String[] arg) throws IOException, ParseException {

//        List<Weather> list = WeatherParser.parseWeather(new Date());
//        for (Weather item : list) {
//            System.out.print(item.getDay() + " ");
//            System.out.println(item.getWeather());
//        }

        System.out.println(new Date().toString());
    }
}
