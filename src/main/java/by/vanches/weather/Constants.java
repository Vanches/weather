package by.vanches.weather;

import java.text.SimpleDateFormat;

/**
 * User: Vanja Novak
 * Date 02.05.2016.
 * Time 17:02.
 */
public interface Constants {

    String WEATHER_API_URL = "http://meteo.by/print/grodno/retro/";

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    SimpleDateFormat REST_FORMAT_DATE = new SimpleDateFormat("MM dd yyyy");

}