package by.vanches.weather.controller;

import by.vanches.weather.Constants;
import by.vanches.weather.model.Weather;
import by.vanches.weather.service.WeatherService;
import by.vanches.weather.utils.WeatherParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * User: Vanja Novak
 * Date: 02.05.2016
 * Time: 22:22
 */
@Controller
@RequestMapping("/api")
public class ApiController {

    public static final String SUCCESS_MESSAGE = "Record added successfully";
    public static final String ERROR_MESSAGE = "An error occurred while adding an entry";

    @Autowired
    WeatherService weatherService;

    @RequestMapping(value = "/data/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Weather>> listAllData() {
        List<Weather> data = weatherService.findByDate(new Date());

        if (data.isEmpty()) {
            return new ResponseEntity<List<Weather>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Weather>>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "/data/{date}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createWeather(@PathVariable String date) {

        Date weatherDate;
        Weather weather;

        try {
            weatherDate = Constants.REST_FORMAT_DATE.parse(date);
            weather = WeatherParser.parseWeather(weatherDate);
        } catch (Exception e) {
            return ResponseEntity.noContent().build();
        }

        if (weather.getDate() != null) {
            weatherService.addWeather(weather);
        } else {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok().build();
    }
}