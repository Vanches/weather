package by.vanches.weather.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;

/**
 * User: Vanja Novak
 * Date: 02.05.2016
 * Time: 22:21
 */
@Controller
@RequestMapping("/")
public class IndexController {

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String getIndexPage() {
        return "index";
    }
}