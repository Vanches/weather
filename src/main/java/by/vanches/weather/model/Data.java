package by.vanches.weather.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

/**
 * User: Vanja Novak
 * Date: 10.05.2016
 * Time: 23:44
 */

@Entity
@Table(name = "Data")
public class Data {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DATA_ID")
    private Long dataId;

    @Enumerated(EnumType.STRING)
    @Column(name = "day")
    private Day day;

    @NotEmpty
    @Column(name = "weather", nullable = false)
    private String weather;

    public Data() {
    }

    public Data(Day day, String weather) {
        this.day = day;
        this.weather = weather;
    }

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }
}
