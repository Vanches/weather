package by.vanches.weather.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * User: Vanja Novak
 * Date: 02.05.2016
 * Time: 0:24
 */
@Entity
@Table(name = "Weather")
public class Weather {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "WEATHER_ID")
    private Long weatherId;

    @DateTimeFormat(pattern = "dd MMM yyyy")
    @Column(name = "date", updatable = false)
    private Date date;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "WEATHER_DATA", joinColumns = {@JoinColumn(name = "WEATHER_ID")},
            inverseJoinColumns = {@JoinColumn(name = "DATA_ID")})
    private Set<Data> dataSet;

    public Weather() {
    }

    public Weather(Date date, Set<Data> dataSet) {
        this.date = date;
        this.dataSet = dataSet;
    }

    public Long getWeatherId() {
        return weatherId;
    }

    public void setWeatherId(Long weatherId) {
        this.weatherId = weatherId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Set<Data> getDataSet() {
        return dataSet;
    }

    public void setDataSet(Set<Data> dataSet) {
        this.dataSet = dataSet;
    }
}
