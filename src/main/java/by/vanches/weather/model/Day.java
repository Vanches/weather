package by.vanches.weather.model;

/**
 * User: Vanja Novak
 * Date 02.05.2016.
 * Time 17:13.
 */
public enum Day {

    NIGHT("ночь"),
    MORNING("утро"),
    DAY("день"),
    EVENING("вечер");

    private final String value;

    public String getValue() {
        return value;
    }

    private Day(String value) {
        this.value = value;
    }

    public static Day findByStringValue(final String value) {

        if (value == null) {
            throw new RuntimeException();
        }

        for (Day val : Day.values()) {
            if (val.getValue().equalsIgnoreCase(value)) {
                return val;
            }
        }
        throw new IllegalArgumentException("Not existing Day value :" + value);
    }
}