package by.vanches.weather.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * User: Vanja Novak
 * Date 02.05.2016.
 * Time 16:40.
 */

public class DateParse {

    public static final Integer TITLE_SIZE_TEXT = 24;

    public static Date parseDateWeather(String title) {
        Date date = null;
        try {
            date = new SimpleDateFormat("dd MMM yyyy", new Locale("ru")).parse(title.substring(TITLE_SIZE_TEXT));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}