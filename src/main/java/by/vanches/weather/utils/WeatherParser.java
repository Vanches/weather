package by.vanches.weather.utils;

import by.vanches.weather.Constants;
import by.vanches.weather.model.Data;
import by.vanches.weather.model.Day;
import by.vanches.weather.model.Weather;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;

/**
 * User: Vanja Novak
 * Date: 02.05.2016
 * Time: 0:46
 */

public class WeatherParser {

    public static Weather parseWeather(Date date) throws IOException {

        Set<Data> data = new HashSet<Data>();

        String url = Constants.WEATHER_API_URL + Constants.dateFormat.format(date);
        Document doc = Jsoup.connect(url).get();

        Elements dateElement = doc.select("#content > .part-nb > .i-sun");

        Elements tableElements = doc.select(".t-weather > tbody > tr");
        for (Element elements : tableElements) {
            Elements tdBlock = elements.select(".time > .temp ");
            Element timeElement = tdBlock.size() != 0 ? tdBlock.get(0) : null;

            if (timeElement == null) {
                continue;
            }
            String time = timeElement.ownText();
            String weather = tdBlock.select("span >  nobr").text();
            if (!time.isEmpty() && !weather.isEmpty()) {
                data.add(new Data(Day.findByStringValue(time), weather));
            }
        }
        return new Weather(DateParse.parseDateWeather(dateElement.text()), data);
    }

}
