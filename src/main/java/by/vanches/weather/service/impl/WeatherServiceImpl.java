package by.vanches.weather.service.impl;

import by.vanches.weather.dao.WeatherDao;
import by.vanches.weather.model.Weather;
import by.vanches.weather.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * User: Vanja Novak
 * Date: 02.05.2016
 * Time: 0:30
 */

@Service("weatherService")
@Transactional
public class WeatherServiceImpl implements WeatherService {

    @Autowired
    private WeatherDao weatherDao;

    public List<Weather> findByDate(Date date) {
        return weatherDao.findByDate(date);
    }

    public Boolean checkWeatherByDate(Date date) {
        return weatherDao.checkWeatherByDate(date);
    }

    public void addWeather(Weather weather) {
        weatherDao.addWeather(weather);
    }

    public List<List<Weather>> weatherList() {
        return null;
    }
}
