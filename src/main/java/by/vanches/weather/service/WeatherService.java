package by.vanches.weather.service;

import by.vanches.weather.model.Weather;

import java.util.Date;
import java.util.List;

/**
 * User: Vanja Novak
 * Date: 02.05.2016
 * Time: 0:29
 */

public interface WeatherService {

    List<Weather> findByDate(Date date);

    Boolean checkWeatherByDate(Date date);

    void addWeather(Weather weather);

    List<List<Weather>> weatherList();

}
