package by.vanches.weather.dao.impl;

import by.vanches.weather.dao.AbstractDao;
import by.vanches.weather.dao.WeatherDao;
import by.vanches.weather.model.Weather;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * User: Vanja Novak
 * Date: 02.05.2016
 * Time: 0:42
 */
@Repository("weatherDao")
public class WeatherDaoImpl extends AbstractDao<Long, Weather> implements WeatherDao {

    public List<Weather> findByDate(Date date) {

        Criteria criteria = getSession().createCriteria(Weather.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        //TODO --
        //criteria.add(Restrictions.eq("date", date));
        return criteria.list();
    }

    public Boolean checkWeatherByDate(Date date) {

//        Criteria criteria = getSession().createCriteria(Weather.class);
//        try {
//            criteria.add(Restrictions.ge("date", Constants.dateFormat.parse(date.getCalendarDate().toString())));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
//        criteria.setProjection(Projections.rowCount());
//        criteria.uniqueResult();
//
//        return (Integer) criteria.uniqueResult() > 0;
        return false;
    }

    @Transactional
    public void addWeather(Weather weather) {
        getSession().save(weather);
    }
}
